from dataclasses import dataclass
from enum import Enum
import numpy as np


NOISY_SERVER_PORT = 51110
NORMAL_SERVER_PORT = 51111
IP_INTERFACE = '127.0.0.1'
BODY_SIZE = np.uint32(2048)
META_SIZE = np.uint32(327)


class DataType(Enum):
    STRING = np.uint8(0)
    IMAGE = np.uint8(1)
    NOISE = np.uint8(2)


@dataclass
class ImageMeta:
    id: str
    type: np.uint8
    n_parts: np.uint32
    part_size: np.uint32
    last_part: np.uint32