import cv2
import numpy as np

from client import Client
from settings import IP_INTERFACE, NOISY_SERVER_PORT

# for i in range(1000):
#     send2sock(IP_INTERFACE, NOISY_SERVER_PORT, f'test {i}')


# img = np.random.randint(0, 255, (100, 100, 3), dtype=np.uint8)
# c = Client()
# c.send2sock_image(IP_INTERFACE, NOISY_SERVER_PORT, img)

img = cv2.imread('data/roxy.jpeg')
c = Client()
c.send2sock_image(IP_INTERFACE, NOISY_SERVER_PORT, img)
