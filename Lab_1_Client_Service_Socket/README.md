# Assignment №1
## Mukhin Artem 6133-010402D

### Launch

The test script can be launched only on unix,
so u can test it via docker for example. 
```bash
pip install -r requirements.txt
bash launch_test.sh
```

### Description

The server is based on `socketserver` module which is included
in the python by default. It's just a simple decorator on
`socket` library. Provides simple and cosy syntax. 

The client uses `socket` module.

Special format of messages were developed to provide a handy
way of sending data to the server. 

The client-side and server-side code, I think is scalable, 
so additional logic can be added eassily. 

Also, I guess there are some possibilities  to improve the 
code, for example by using *asynchronous programming* to make 
the **client** works a little bit **faster**.

### Result

#### Original image
![original](original.png)
#### Noised image
![noise](noise.png)
#### Denoised image
![denoise](result.png)