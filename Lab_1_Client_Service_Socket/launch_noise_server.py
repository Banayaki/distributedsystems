from server import NoisyServer


server = NoisyServer()
server.start_server()
try:
    server.loop()
finally:
    server.stop_server()