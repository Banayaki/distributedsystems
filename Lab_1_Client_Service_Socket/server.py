import io
import logging
from copy import copy
from random import random
from tkinter import image_types
from typing import List, Optional, Tuple

import cv2
import numpy as np

from base_server import ThreadedTCPServer
from client import Client
from settings import (IP_INTERFACE, NOISY_SERVER_PORT, NORMAL_SERVER_PORT,
                      DataType)

logging.basicConfig(level=logging.DEBUG)


def load_image(bytes_list: List[bytes]) -> np.ndarray:
    """
        Converts bytes to numpy array
    """
    image_bytes = bytes()
    for b in bytes_list:
        image_bytes += b
    return np.load(io.BytesIO(image_bytes))


class NormalServer(ThreadedTCPServer):
    """
        Implements real server logic.
        Performs denoising the received image and compare it with 
        original one using MSE metric.
    """

    def __init__(self):
        super().__init__(ip=IP_INTERFACE, port=NORMAL_SERVER_PORT)
        self.image_bytes_list: List[bytes] = []
        self.noisy_bytes_list: List[bytes] = []

        self.client = Client()
        # I were playing with asyncio
        self.logger = logging.getLogger("asyncio")

    def handle(self, data: Optional[Tuple[bytes, int]]):
        image_bytes, image_type = data
        if image_bytes is not None:
            if image_type == DataType.IMAGE:
                self.image_bytes_list.append(image_bytes)
            else:
                self.noisy_bytes_list.append(image_bytes)
        else:  # When both the images are gethered 
            if len(self.image_bytes_list) == len(self.noisy_bytes_list):
                self._process_images(self.image_bytes_list,
                                     self.noisy_bytes_list)
                self.noisy_bytes_list = []
                self.image_bytes_list = []

    def _process_images(self, image_bytes: List[bytes], noisy_bytes: List[bytes]):
        """
            Performs denoising, computes MSE and saves the result
        """
        image = load_image(image_bytes)
        noisy_image = load_image(noisy_bytes)
        denoised_image = self._denoise_image(noisy_image)
        dif = self._image_difference_metric(image, denoised_image)

        cv2.imwrite('result.png', denoised_image)
        cv2.imwrite('noise.png', noisy_image)
        cv2.imwrite('original.png', image)

        self.logger.info(f'Difference between images in terms of MSE = {dif}')

    def _denoise_image(self, image: np.ndarray) -> np.ndarray:
        return cv2.medianBlur(image, ksize=5)

    def _image_difference_metric(self, img_1: np.ndarray, img_2: np.ndarray) -> float:
        return np.mean(np.square(img_1 - img_2))


class NoisyServer(ThreadedTCPServer):
    """
        Implements middleware logic. Adds salt and papper noise 
        into the recieved image
    """

    def __init__(self):
        super().__init__(ip=IP_INTERFACE, port=NOISY_SERVER_PORT)
        self.image_bytes_list = []

        self.client = Client()
        # I were playing with asyncio
        self.logger = logging.getLogger("asyncio")

    def handle(self, data: Optional[Tuple[bytes, int]]):
        image_bytes, _ = data
        if image_bytes is not None:
            self.image_bytes_list.append(image_bytes)
        else:  # Then all the data is gethered
            self._handle_image(self.image_bytes_list)
            self.image_bytes_list = []

    def _handle_image(self, image_bytes_list: List[bytes]):
        """
            Adds noise to the image and send both noised and original image 
            to the "real" server
        """
        image = load_image(image_bytes_list)
        noisy_image = image.copy()
        self._process_image(noisy_image)

        self.client.send2sock_image(
            IP_INTERFACE, NORMAL_SERVER_PORT, image, msgtype=DataType.IMAGE)
        self.client.send2sock_image(
            IP_INTERFACE, NORMAL_SERVER_PORT, noisy_image, msgtype=DataType.NOISE)

    def _process_image(self, image: np.ndarray):
        """
            Kinda salt and pepper noise
        """
        h, w, c = image.shape
        for ih in range(h):
            for iw in range(w):
                if random() > 0.9:
                    image[ih, iw] = int(random() > 0.5) * 255
