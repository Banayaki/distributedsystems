#!/bin/bash

FILE=./original.png

rm *.png
echo "Starting test script"

python3 launch_noise_server.py &
python3 launch_normal_server.py &

sleep 1

python3 test.py

until [ -f $FILE ]
do 
    sleep 0.5
done
echo "Test is completed"
echo "Killing the servers"

kill $(jobs -p)
sleep 1

echo "Check both servers are killed"
jobs