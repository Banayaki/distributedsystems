import pickle
import socket
import threading
from abc import abstractmethod
from queue import Queue
from socketserver import BaseRequestHandler, TCPServer, ThreadingMixIn

from settings import META_SIZE, DataType, ImageMeta


class RequestHandler(BaseRequestHandler):
    """
        Handler which is used by server.
        Just takes a data from the socket, reads it and puts into the Queue
    """

    def handle(self):
        """
            Queue takes tuple of (data, data_type)
        """
        # Recieve meta infromation (type of data, amount of data, etc.)
        meta: ImageMeta = pickle.loads(self.request.recv(META_SIZE, socket.MSG_WAITALL))
        # print(threading.current_thread(), hash(self.server.queue))
        # May be expanded to handle other types of data
        if meta.type in [DataType.IMAGE, DataType.NOISE]:
            for _ in range(meta.n_parts):
                data = self.request.recv(meta.part_size, socket.MSG_WAITALL)
                self.server.add((data, meta.type))
            self.server.add(
                (self.request.recv(meta.last_part, socket.MSG_WAITALL), meta.type)
            )
            self.server.add((None, None))


class ThreadedTCPServer(ThreadingMixIn, TCPServer):
    """
        Basicly a socket server implementation from the documentation:
        https://docs.python.org/3/library/socketserver.html

        There is a good idea to create one queue for each connection and
        fetches the data using async/await, but the server
        can manage only one connection simultaneously
    """

    def __init__(self, ip: str, port: int):
        super().__init__((ip, port), RequestHandlerClass=RequestHandler)
        # TODO: Now simultanious multiuser processing is unavailable
        self.queue = Queue()

        # server_thread just calls `handle` method from RequestHandler
        # for each incoming request
        self.server_thread = threading.Thread(target=self.serve_forever)
        self.server_thread.daemon = True

    def loop(self):
        while True:
            # time.sleep(0.01)
            while not self.queue.empty():
                self.handle(self.queue.get())

    @abstractmethod
    def handle(self, message: str):
        pass

    def start_server(self):
        self.server_thread.start()
        print('Server is running')

    def stop_server(self):
        self.shutdown()
        self.server_close()
        print('Serve has stopped')

    def add(self, message: str):
        self.queue.put(message)

    def get(self):
        return self.queue.pop()
