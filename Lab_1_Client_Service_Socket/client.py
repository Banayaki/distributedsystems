import io
import pickle
import socket
from dataclasses import dataclass
from uuid import uuid4

import numpy as np

from settings import BODY_SIZE, DataType, ImageMeta


class Client:
    """
        Socket client implementation
    """

    def __init__(self):
        # Used to determine client
        self.id = uuid4()

    def send2sock_image(self, ip: str, port: int, image: np.ndarray, msgtype: int = DataType.IMAGE):
        """
            Computes all necessary metadata including type and shape of the bytes array
            Converts numpy array to bytes using np.load method (adds header which stores
            an information about shape and type) and sends the data to the server.

            If something goes wrong throws socket's runtime exception such PipeBroken
        """
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            image = image.astype(np.uint8, copy=False)
            byte_image = io.BytesIO()
            # Adds header to the file which contains information about the shape and dtype
            np.save(byte_image, image)
            n_iters = len(byte_image.getvalue()) // BODY_SIZE
            last_part_size = len(byte_image.getvalue()) - BODY_SIZE * n_iters

            meta = ImageMeta(
                id=str(self.id),
                type=msgtype,
                n_parts=np.uint32(n_iters),
                part_size=BODY_SIZE,
                last_part=np.uint32(last_part_size)
            )
            encoded_meta = pickle.dumps(meta)  # may be substituded with json, yaml formats
            sock.connect((ip, port))

            sock.sendall(encoded_meta)
            for i in range(n_iters):
                body = byte_image.getvalue()[BODY_SIZE * i: (i + 1) * BODY_SIZE]
                sock.sendall(body)
            body = byte_image.getvalue()[-last_part_size:]
            sock.sendall(body)
