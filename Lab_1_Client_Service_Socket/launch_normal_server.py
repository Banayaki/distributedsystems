from server import NoisyServer, NormalServer


server = NormalServer()
server.start_server()
try:
    server.loop()
finally:
    server.stop_server()