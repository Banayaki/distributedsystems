from typing import Optional

from p2p.model import ChordNode
from p2p.model import Node


class NodeLocalDao:
    """
    Give an access to the `ChordNode` attributes
    """

    def __init__(self, chord_node: ChordNode):
        self.chord_node = chord_node

    def get_node(self) -> Node:
        return self.chord_node.node

    def get_predecessor(self):
        return self.chord_node.predecessor

    def get_successor(self):
        return self.chord_node.successor

    def get_fingers(self):
        return self.chord_node.fingers

    def set_successor(self, successor_node: Optional[Node]):
        self.chord_node.successor = successor_node

    def set_predecessor(self, predecessor_node: Optional[Node]):
        self.chord_node.predecessor = predecessor_node

    def set_finger(self, idx: int, finger_node: Optional[Node]):
        self.chord_node.fingers[idx] = finger_node
