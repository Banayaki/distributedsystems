import pickle
from typing import Optional

import rpyc

from p2p.dao import NodeLocalDao
from p2p.model import Node
from p2p.services import NodeClient
from p2p.utils.exceptions import DisconnectedNodeException


class NodeRemoteDao(rpyc.Service):
    """
    Extends ``rpyc.Service`` in ordred to provide a remote access to ``ChordNode``.
    Prefix ``exposed_`` is a default prefix for methods shared via RPC.

    Notes
        I'm using pickle's serialization because ``rpyc`` proxies all the objects he's managing.
        Only primitives don't proxy and send by value.
        See Also https://rpyc.readthedocs.io/en/latest/docs/theory.html

        DANGEROUS:
        In case of check disconnected nodes in that class the ``NodeClient`` is using.
        It may cause circular import issue in some cases (I can feel it), so be carefull
    """

    def __init__(self, node: NodeLocalDao):
        self.node = node

    def exposed_get_node(self) -> bytes:
        return pickle.dumps(self.node.get_node())

    def exposed_get_predecessor(self) -> bytes:
        predecessor = self.node.get_predecessor()

        predecessor = self._check_availablity(predecessor)
        if predecessor is None:  # Predecessor is unavailable or it is None
            self.node.set_predecessor(None)

        return pickle.dumps(predecessor)

    def exposed_get_successor(self) -> bytes:
        successor = self.node.get_successor()

        successor = self._check_availablity(successor)
        if successor is None:  # Successor is unavailable
            self.node.set_successor(self.node.get_node())

        return pickle.dumps(self.node.get_successor())

    def exposed_get_fingers(self) -> bytes:
        fingers = self.node.get_fingers()

        for idx, finger in enumerate(fingers):
            finger = self._check_availablity(finger)
            if finger is None:
                self.node.set_finger(idx, None)
                if idx == 0:
                    self.node.set_successor(self.node.get_node())

        return pickle.dumps(self.node.get_fingers())

    def exposed_set_successor(self, successor_node: bytes):
        self.node.set_successor(pickle.loads(successor_node))

    def exposed_set_predecessor(self, predecessor_node: bytes):
        self.node.set_predecessor(pickle.loads(predecessor_node))

    def exposed_set_finger(self, idx: int, finger_node: bytes):
        self.node.set_finger(idx, pickle.loads(finger_node))

    def _check_availablity(self, node: Optional[Node]) -> Optional[Node]:
        if node is None:
            return None

        client = NodeClient(node.ip, node.port)
        try:
            client.get_node()
            return node
        except DisconnectedNodeException:
            return None
