from dataclasses import dataclass
from typing import Optional

from p2p.settings import NUMBER_OF_BITS, NUMBER_OF_BACKUPS
from p2p.utils import hash_string


@dataclass(repr=True)
class Node:
    """
    Domain class which stores basic node information

    Attributes:
        ip: stores ip address for example 192.168.10.3, localhost
        port: just a port address
        key: string from which the hash will be generated
        address: field which contains combination of ip:port
        node_id: hashed key
    """
    ip: str
    port: int
    address: str
    key: str
    node_id: int

    def __init__(self, ip: str, port: int, key: str):
        self.ip = ip
        self.port = port
        self.key = key

        self.node_id = hash_string(key)
        self.address = f'{ip}:{port}'


@dataclass(repr=True, init=False)
class ChordNode:
    """
    Domain class which stores node's successor, predecessor and fingers

    Attributes
        node: node by itself
        successor: node's successor by default is None
        predecessor: node's predecessor by default is None
        fingers: node's fingers by default is list of Nones
        backup_nodes: nodes that being used if successor is unavailable

    Notes
        TODO: data storage also is needed to be here? Kinda of dictionary {'resource_hash': 'resource'} ?
    """
    node: Optional[Node] = None
    successor: Optional[Node] = None
    predecessor: Optional[Node] = None
    fingers = [None] * NUMBER_OF_BITS
    backup_nodes = [None] * NUMBER_OF_BACKUPS
