import sys
from typing import Optional, Union

from p2p.model import Node
from p2p.services import NodeClient
from p2p.settings import NUMBER_OF_BITS
from p2p.utils.exceptions import DisconnectedNodeException


def check_id_in_circular_interval(start, stop, value, include_stop=False) -> bool:
    """
    Check that value is in between clockwise interval from start to stop

    Args:
        start: left border of the interval
        stop: right border of the interval
        value: value to be chekced
        include_stop: include or exclude the ``stop`` value

    Returns:
        True if value within a range
    """
    if start == stop:
        return True
    if include_stop:
        if stop < start:
            return start < value <= 2 ** NUMBER_OF_BITS or 0 <= value < stop
        return start < value < stop
    else:
        if stop < start:
            return start < value <= 2 ** NUMBER_OF_BITS or 0 <= value <= stop
        return start < value <= stop


class Chord:
    """
    Realization of chord algorithm

    Notes:
        Current realization is inspired by the following paper:
        https://pdos.csail.mit.edu/papers/ton:chord/paper-ton.pdf

        Could with even with RPC, local, socket's implementation of ``NodeClient`` I think

    References:
        https://pdos.csail.mit.edu/papers/ton:chord/paper-ton.pdf
    """

    def __init__(self, node_to_serve: NodeClient, existing_node: Optional[NodeClient] = None):
        self.node_to_serve = node_to_serve
        if existing_node is None:
            self.create()
        else:
            self.join(existing_node)

    def create(self):
        """
        Called in the node which are the first in the chord ring, i.e. there are no
        existing nodes in the ring
        """
        # self.node_to_serve.set_predecessor = None
        current_state = self.node_to_serve.get_node()
        self.node_to_serve.set_successor(current_state)

    def join(self, existing_node: NodeClient):
        """
        Called in the node which are attempting to join the chord ring
        The ``existing_node`` must be provided
        """
        # self.node_to_serve.set_predecessor = None
        try:
            successor = self.find_successor(existing_node, self.node_to_serve)
            self.node_to_serve.set_successor(successor)
        except DisconnectedNodeException:
            print("Try again using another peer node", file=sys.stderr)
            exit(-1)

    def find_successor(self, find_from: NodeClient, find_for: Union[NodeClient, int]) -> Node:
        """
        Searches for the successor (next node) for the ``find_for`` node in the ``find_from`` node.

        Args:
            find_from: the node in which the successor will be searching
            find_for: the node for which the successor is searching

        Returns:
            Hypotethical successor's node
        """
        from_node = find_from.get_node()
        from_node_successor = find_from.get_successor()

        if isinstance(find_for, NodeClient):
            find_for_node_id = find_for.get_node().node_id
        else:
            find_for_node_id = find_for

        if check_id_in_circular_interval(
                from_node.node_id,
                from_node_successor.node_id,
                find_for_node_id,
                include_stop=True):
            return from_node_successor

        closest_node = self.closest_preceding_node(find_from, find_for_node_id)
        if closest_node.node_id == from_node.node_id:
            return from_node

        return self.find_successor(
            self._wrap_with_connection(closest_node),
            find_for
        )

    def closest_preceding_node(self, find_from: NodeClient, find_for_node_id: int) -> Node:
        """
        Find the closest preceding node to the node with id ``find_for_node_id`` using fingers table
        """
        from_node = find_from.get_node()
        fingers = find_from.get_fingers()
        for finger in fingers:
            if finger is not None and \
                    check_id_in_circular_interval(
                        from_node.node_id,
                        find_for_node_id,
                        finger.node_id):
                return finger
        return from_node

    def stabilize(self):
        """
        Performs stabilization procedure which updates node's successor and notifies
        another node to check current node to be it's predecessor
        """
        current_node = self.node_to_serve.get_node()
        successor = self.node_to_serve.get_successor()
        successors_predecessor = self._wrap_with_connection(successor).get_predecessor()
        if successors_predecessor is not None and check_id_in_circular_interval(current_node.node_id, successor.node_id,
                                                                                successors_predecessor.node_id):
            self.node_to_serve.set_successor(successors_predecessor)
        self.notify(self._wrap_with_connection(successor), self.node_to_serve)

    def notify(self, node_to_notify: NodeClient, node_to_check: NodeClient):
        """
        Performs check that ``node_to_check`` should be node's predecessor
        """
        notified_node = node_to_notify.get_node()
        checked_node = node_to_check.get_node()
        predecessor = node_to_notify.get_predecessor()
        if predecessor is None or check_id_in_circular_interval(predecessor.node_id, notified_node.node_id,
                                                                checked_node.node_id):
            node_to_notify.set_predecessor(checked_node)

    def fix_fingers(self, finger_idx: int):
        """
        Performs setupping fingers table, also update the successor if ``finger_idx == 0``
        """
        current_node = self.node_to_serve.get_node()
        proposed_successor = self.find_successor(
            self.node_to_serve,
            (current_node.node_id + 2 ** finger_idx) % 2 ** NUMBER_OF_BITS
        )

        if current_node.node_id != proposed_successor.node_id:
            self.node_to_serve.set_finger(finger_idx, proposed_successor)
            if finger_idx == 0:
                self.node_to_serve.set_successor(proposed_successor)

    def _wrap_with_connection(self, node: Node) -> NodeClient:
        return NodeClient(node.ip, node.port)
