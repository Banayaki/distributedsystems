import time
from typing import Optional

from p2p.services import NodeClient
from p2p.settings import NUMBER_OF_BITS
from p2p.utils import run_in_thread
from p2p.utils.exceptions import DisconnectedNodeException
from .Chord import Chord


class ChordRunner:
    """
    Starts chord algorithm, i.e. creates a chord node and schedules stabilization and fix fingers tasks
    for the ``target_node``

    Attributes:
        target_node: node which will be a served by this runner
        peer_node: existing in the chord ring node to be connected from
        chord: service providing chord algorithm implementation
    """
    target_node: NodeClient
    peer_node: Optional[NodeClient] = None
    chord: Chord

    def set_target_node(self, ip: str, port: int):
        self.target_node = NodeClient(ip, port)
        return self

    def set_peer_node(self, ip: str, port: int):
        self.peer_node = NodeClient(ip, port)
        return self

    def run(self):
        self.chord = Chord(node_to_serve=self.target_node, existing_node=self.peer_node)
        self.schedule_stabilization_task()
        self.schedule_fix_fingers_task().join()

    def schedule_stabilization_task(self):
        TIMEOUT = 1

        def stabilize(self):
            while True:
                try:
                    self.chord.stabilize()
                except DisconnectedNodeException:
                    print("DisconnectedNodeException in stabilize.", file=sys.stderr)
                time.sleep(TIMEOUT)

        return run_in_thread(stabilize, self)

    def schedule_fix_fingers_task(self):
        TIMEOUT = 1

        def fix_fingers(self):
            finger_idx = 0
            while True:
                try:
                    self.chord.fix_fingers(finger_idx)
                except DisconnectedNodeException:
                    print("DisconnectedNodeException in fix_fingers.", file=sys.stderr)
                time.sleep(TIMEOUT)
                finger_idx = finger_idx + 1 if finger_idx < NUMBER_OF_BITS - 1 else 0

        return run_in_thread(fix_fingers, self)


if __name__ == '__main__':
    import sys

    port = 50000 if len(sys.argv) == 1 else 50000 + int(sys.argv[1])

    runner = ChordRunner().set_target_node('localhost', port)
    if len(sys.argv) == 2:
        runner.set_peer_node('localhost', 50000)
    runner.run()
