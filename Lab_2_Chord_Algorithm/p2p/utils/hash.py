from hashlib import sha1

from p2p.settings import NUMBER_OF_BITS


def hash_string(string_to_hash: str) -> int:
    hash_algorithm = sha1()
    hash_algorithm.update(bytes(string_to_hash, "ASCII"))
    return int(hash_algorithm.hexdigest(), 16) % 2 ** NUMBER_OF_BITS
