import threading
import time
from functools import partial


def run_function(timeout, func):
    """
    Depricated
    """
    while True:
        print("here", flush=True)
        ret_val = func()
        time.sleep(timeout)


def repeat_by_timeout(timeout: int, *args, **kwargs):
    """
    Depricated
    """

    def decorator(func):
        print("func", func)
        print("args", args)
        print("args", kwargs)
        threading.Thread(
            target=partial(run_function, timeout=timeout, func=func),
            daemon=True,
        ).start()

    return decorator


def run_in_thread(func, *args, **kwargs) -> threading.Thread:
    """
    Run specified function with passed arguments in separate daemon thread.
    Usefull in task scheduling.

    Args:
        func: function to be launched in separate thread
        *args: function's args
        **kwargs: function's kwargs

    Returns:
        threading.Thread object of launched thread.
    """
    th = threading.Thread(target=func, args=args, kwargs=kwargs, daemon=True)
    th.start()
    return th
