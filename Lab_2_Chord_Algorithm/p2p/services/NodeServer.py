import os
import time
from datetime import datetime

from rpyc import ThreadedServer

from p2p.dao import NodeRemoteDao, NodeLocalDao
from p2p.model import ChordNode
from p2p.model import Node
from p2p.utils import run_in_thread


class NodeServer:
    """
    Class that instantinates the ``ChordNode`` on specified ip and port
    May be considered as remote __Data Source__ available at specified address
    and working via RPC protocol
    """

    def __init__(self, chord_node: ChordNode):
        self.local_node = NodeLocalDao(chord_node=chord_node)
        self.remote_node = NodeRemoteDao(self.local_node)

    def start(self):
        """
        Starts a ``ThreadedServer`` which opens a new thread for each incoming request.
        Also starts scheduled ``ping`` task which prints in the stdout node's current state.
        """
        print('Starting server')

        self.start_ping()

        ThreadedServer(
            self.remote_node,
            hostname=self.local_node.get_node().ip,
            port=self.local_node.get_node().port) \
            .start()

    def start_ping(self):
        TIMEOUT = 5

        def ping(self):
            while True:
                print(datetime.now().strftime("%H:%M:%S"))
                print('Node', self.local_node.get_node())
                print('Successor', self.local_node.get_successor())
                print('Predecessor', self.local_node.get_predecessor())
                print('Fingers', list(
                    map(lambda item: item.node_id if item is not None else '', self.local_node.get_fingers())))
                print('-' * 20)
                time.sleep(TIMEOUT)
                os.system('cls' if os.name == 'nt' else 'clear')

        run_in_thread(ping, self)


if __name__ == '__main__':
    import sys

    port = 50000 if len(sys.argv) == 1 else 50000 + int(sys.argv[1])
    node = Node(ip='localhost', port=port, key='test' + str(port))
    chord_node = ChordNode()
    chord_node.node = node

    NodeServer(chord_node).start()
