import pickle
import sys
import traceback
from contextlib import contextmanager

import rpyc

from p2p.model import Node
from p2p.utils.exceptions import DisconnectedNodeException


@contextmanager
def connect_to_remote_node(ip, port):
    """
    Opens a connection by specified ip and port to the remote node

    Args:
        ip: for a node is needed to be connected with
        port: for the node is needed to be connected with

    Yields:
        NodeRemoteDao

    Notes:
        Using ``BgServingThread`` is unnessecary here, but let it be with no reasons
    """
    try:
        connection = rpyc.connect(ip, port)
        bkg_connection = rpyc.BgServingThread(connection)
        node = connection.root
    except Exception as e:
        traceback.print_exc()
        print('Can not connect to the remote node by the following address', file=sys.stderr)
        print(f'Address: {ip}:{port}', file=sys.stderr)

        raise DisconnectedNodeException()

    try:
        yield node
    except Exception as e:
        traceback.print_exc()
        print('Disconnected from the remote node by the following address', file=sys.stderr)
        print(f'Address: {ip}:{port}', file=sys.stderr)

        raise DisconnectedNodeException()
    finally:
        # node.stop()
        bkg_connection.stop()
        connection.close()


class NodeClient:
    """
    Client for RPC methods invocation.
    Open short-time connection for each request via context manager.
    Serializing all the object using ``pickle`` library (See also ``NodeRemoteDao.py``).

    By the way this class is mostly a wrapper on NodeRemoteDao provides an access to the
    RPC service by specified ip and port.
    """

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def get_node(self) -> Node:
        with connect_to_remote_node(self.ip, self.port) as node_dao:
            node = pickle.loads(node_dao.get_node())
        return node

    def get_predecessor(self):
        with connect_to_remote_node(self.ip, self.port) as node_dao:
            return pickle.loads(node_dao.get_predecessor())

    def get_fingers(self):
        with connect_to_remote_node(self.ip, self.port) as node_dao:
            return pickle.loads(node_dao.get_fingers())

    def get_successor(self):
        with connect_to_remote_node(self.ip, self.port) as node_dao:
            return pickle.loads(node_dao.get_successor())

    def set_successor(self, successor_node: Node):
        with connect_to_remote_node(self.ip, self.port) as node_dao:
            node_dao.set_successor(pickle.dumps(successor_node))

    def set_predecessor(self, predecessor_node: Node):
        with connect_to_remote_node(self.ip, self.port) as node_dao:
            node_dao.set_predecessor(pickle.dumps(predecessor_node))

    def set_finger(self, idx: int, finger_node: Node):
        with connect_to_remote_node(self.ip, self.port) as node_dao:
            node_dao.set_finger(idx, pickle.dumps(finger_node))
