# Assignment №2 Chord Algorithm

## Mukhin Artem 6133-010402D

### Launch _data_ node

```bash
python3 -m p2p.services.NodeServer <port_increment>
```

### Launch _chord_ node

```bash
python3 -m p2p.chord.ChordRunner <port_increment>
```

I recommend to launch the nodes via `tmux`. Or to write a web app :d

### Implementation detail

It is worth to mentioning that I implemented chord node using the real RPC protocol. So each node works in separate
process instead one single process via multithreading (hi GIL). I think this is the most correct implementation of the *
Chord algorithm* that can be implemented in this course.

Two separate node was developed in order to avoid deadlocks which ussualy appears if both data and logic stays in single
process. Therefore I've separated the data and the chord algorithm into to separate modules:

* `ChordRunner`
* `NodeServer`

This approach allows to evaid most of the threading issues such as deadlock and race.
`NodeServer` or **Data Node** is using as datasource, hance I've implemented two DAOs (Local and Remote one).
`ChordRunner` is just a process which runs chord's methods and algorithms by schedule.

Both modules could be composed into one **P2P client** which provides an access to the Chord Ring. But this thing is
outside of scope of current assignment.

Moreover I've somehow implemented ring stability when nodes are disconecting. Basically, the ring is robust and can "
repair" by itself, so I just add some try\catch blocks which restarts the shecduled methods after failure. And "Is this
node alive?" check by attempting to connect, so simple :D.

#### It works O_O

![img.png](img.png)
